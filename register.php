
<?php
$gender = array(0 => "Nam", 1 => "Nữ"); 
$department = array("MAT" => "Khoa học máy tính","KDL" => "Khoa học dữ liệu");
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="register.css">
</head>

<body>
    <div class='login-content'>

        <form class='form-input'>
            <div class="content">
                <div class='lb'>
                    <label for='luname'>Họ và tên </label>
                </div>
                <input type='text' class='input-item'>
            </div>
            <div class="content">
                <div class="lb">
                    <label for='luname'>Giới tính</label>
                </div>
                <div id="radio-content">
                    <?php
                    for($x = 0; $x <= 1; $x++){
                        echo("
                             <input type=\"radio\"  name=\"gender\" >$gender[$x]</input>
                        ");
                    }
                       
                    ?>
                </div>

            </div>
            <div class="content">
                <div class="lb">
                    <label for='lpassword'>Phân khoa</label>
                </div>
                <select name="phan-khoa" class="select-item">
                <?php
                    echo("
                        <option ></option>
                    ");
                    foreach($department as $value ){
                        echo("
                            <option >$value</option>
                        ");
                    }
                    ?>
                </select>
            </div>

            <input type='submit' value='Đăng ký' id='btn-submit'>
        </form>


    </div>

</body>

</html>